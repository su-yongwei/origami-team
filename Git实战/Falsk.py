from flask import Flask
from mysql import Mysql
from flask import *

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def getdata():
    db = Mysql()
    items = db.getItems()
    return render_template('main.html', items=items)


@app.route('/search', methods=['GET', 'POST'])
def search_name():
    db = Mysql()
    name = request.values['name']
    items = db.getName(name)
    if len(items) == 0:
        return "null"
    else:
        return render_template('main.html', items=items)


@app.route('/delete', methods=['GET', 'POST'])
def del_name():
    db = Mysql()
    name = request.values['name']
    db.delName(name)
    items = db.getItems()
    return render_template('main.html', items=items)


if __name__ == '__main__':
    app.run(debug=True)  # debug=True发生错误时会返回发生错误的地方
