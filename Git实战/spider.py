import time
import requests

# 获取数据
from mysql import Mysql


def getData():
    # 浏览器标识
    headers = {
        'User-Agent': 'Mozilla/5.0(WindowsNT6.1;WOW64)AppleWebKit/537.36(KHTML,likeGecko)Chrome/53.0.2785.143Safari/537.36MicroMessenger/7.0.9.501NetType/WIFIMiniProgramEnv/WindowsWindowsWechat'
    }
    url = 'https://j1.pupuapi.com/client/product/storeproduct/detail/4dcdeca2-f5a3-4be8-9e2f-e099889a23a0/d588f482-7e71-4864-8644-f8ebd44f2154'
    r = requests.get(url, headers=headers).json()
    return r

# 解析JSON获取相应的值
def getProduct():
    response = getData()
    pid = response['data']['id']  # 获取商品id
    # 匹配商品id
    # if pid == '081bc7dd-bcba-42d4-9475-230f7683d413':
    # 商品
    name = response['data']['name']
    # 详细内容
    content = str(response['data']['share_content'])
    # 折后价
    price = str(response['data']['price'] / 100)
    # 原价
    market_price = str(response['data']['market_price'] / 100)
    # 规格
    spec = str(response['data']['spec'])
    share_content = response['data']['share_content']  # 获取简介
    origin = response['data']['origin']  # 获取原产地
    data = {
        'name': name,
        'price': price,
        'market_price': market_price
    }
    print(data)
    # 打印输出商品信息
    db = Mysql()
    db.inset(data)
    print('---------------' + '商品: ' + name + '---------------')
    print('规格:' + spec)
    print('价格: ' + price)
    print('原价/折扣价: ' + market_price + '/' + price)
    print('详细内容: ' + share_content)
    print('原产地是：' + origin)


# else:
#     print('未找到对应的商品')


# 统一模块化
def run():
    getProduct()


# 运行函数
run()
