import pymysql


class Mysql(object):
    def __init__(self):
        try:
            self.conn = pymysql.connect(host='localhost', user='root', password='123456', database='pupu',
                                        charset="utf8")
            self.cursor = self.conn.cursor()  # 用来获得python执行Mysql命令的方法（游标操作）
            print("连接数据库成功")
        except:
            print("连接失败")

    def getItems(self):
        sql = "select * from data"  # 获取food数据表的内容
        self.cursor.execute(sql)
        items = self.cursor.fetchall()  # 接收全部的返回结果行
        return items

    def getName(self, name):
        sql = "select * from data where name REGEXP '%s'" % (name)
        self.cursor.execute(sql)
        items = self.cursor.fetchall()
        return items

    def delName(self, name):
        sql = "delete from data where name = '%s'" % (name)
        self.cursor.execute(sql)
        self.conn.commit()
        items = self.cursor.fetchall()
        return items

    def inset(self, data):
        sql = "insert into data value ('%s',%s,%s)" % (data['name'], data['price'], data['market_price'])
        self.cursor.execute(sql)
        self.conn.commit()
        items = self.cursor.fetchall()
        return items
