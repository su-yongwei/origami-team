# 第一章 引言



## 1.1目的

《软件需求规格说明书》主要是为*二手书小程序*所撰写的需求规格说明书，本说明书在于清晰地指导最终用户、开发者完成的目标，描述项目系统的功能结构。

## 1.2背景

软件名称：二手书

开发者：福州大学至诚学院计算机工程系2021级 

本项目经过了用户需求问卷调研，针对现有资源浪费情况分析：大部分学生会在期末的时候将书籍丢弃。




## 1.3预期读者

（1）项目经理：根据此文档进行系统设计，项目管理

（2）程序员：根据该文档对二手书进行开发工作


（3）测试员：根据文档对该产品进行测试用例

（4）用户：了解产品的功能





## 1.4参考资料

- 《GB9385-2008 计算机软件需求规格说明规范》

- 《GB9386-2008 计算机软件测试文档编制规范》

- 《构建之法》第三版 作者：邹欣





# 第四章 功能需求描述



## 4.1 登录

- 登录时，用户名由3~20个字母、数字或者“_”组成，密码必须包含字母和数字，长度大于8。
- 登录成功后可从主窗口菜单中，进行切换账号和修改密码。



## 4.2 上架书籍

- 通过资料填写书籍信息，磨损程度，上架价格。


## 4.3 购买书籍

- 用户能通过购买的书籍自动跳转到付款界面。



## 4.4 聊天

- 能够买卖方进行交易的同时进行交流。




## 4.5 广告出租

- 能够点击广告跳转到广告页面



## 4.6 搜索功能
- 书籍搜索的精准度，和搜索跳转



# 第五章:非功能需求



## 5.1性能需求

支持全体师生进行书籍上架与购买。



## 5.2软件属性

### 5.2.1数据精确度

- 搜索到的数据精准匹配
- 发货地址精准定位

### 5.2.2软件可靠性

- 需要安全认证不存在，买方购买，卖方不发货现象
- 正常退出时不存在数据丢失

### 5.2.3可用性

- 并发性：支持多用户者同时购买与上架。
- 页面界面简洁。
- 数据图形显示清晰。




# 第六章:验收验证标准



## 6.主要功能点

项目主要功能验收清单如下：

| 序号 | 功能点 |   功能实现   |        备注        |
| --   | ----  | ----------  | ----------------  |
|  1   |  登录  |   用户登录   | 密码登录或验证码登录 |
|  2   |  上架  |   上架书籍   |      正常上架     |
|  3   |  购买  |   购买书籍   |      正常购买      |
|  4   |  搜索  | 搜索特定书籍 |         无         |



## 6.2界面效果

软件界面；大方整洁。


# 第七章:未来期望完成的功能



## 7.1扩大用户范围

第一步是针对本校师生用户，后期将扩大至全国用户


## 7.2解决“前期推广”的问题：

推广尽量往二手群里推广，qq群，微信群等





## 7.3针对未来可能的面向商业市场的计划

### 7.3.1具体设计方案

1. 只有广告位放置广告，不需要频繁的弹出广告。
2. 安全性能进一步完善，尽量避免有外者入侵就崩溃
3. 根据创建时间来进行优惠券的获取


### 7.5.2上述方案的好处

1. 频繁的弹出广告会使用户的体验极差，只有广告位有广告所有人都能理解性，从而减少用户体验。

2. 完善安全性能是一款软件的初始，没有安全性能只会导致软件无时不刻被攻击就崩溃。

3.根据创建时间来进行分配，提高前期用户的创建量。